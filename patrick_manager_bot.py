from telegram.ext import Updater, CommandHandler
import random
import logging

def start(update, ctx):
    update.message.reply_text("Hello! Use /add <name> to add a player, /remove <name> to remove a player,\
                              /draw to extract names at random. The bowl always has as many 'double' as half the players",
                              quote=False)

def get_bowls(ctx):
    d = ctx.chat_data
    if "bowl1" not in d:
        d["bowl1"] = []
        d["bowl2"] = []
    return d["bowl1"], d["bowl2"]

def reset_bowls(ctx):
    bowl1, bowl2 = get_bowls(ctx)
    bowl1.extend(bowl2)
    bowl2[:] = []
    return bowl1, bowl2

def add_player(update, ctx):
    bowl1, bowl2 = reset_bowls(ctx)

    command_and_name = update.message.text.strip().split(" ")
    if len(command_and_name) < 2 or command_and_name[1] == "" or command_and_name[1] == "double":
        update.message.reply_text("please send me a name after /add")
        return

    name = command_and_name[1]
    if name in bowl1:
        update.message.reply_text("'{}' is already playing".format(name))
    else:
        bowl1.append(name)
        update.message.reply_text("added player '{}'".format(name))
        n_doubles = bowl1.count("double")
        if n_doubles < (len(bowl1) - n_doubles) // 2:
            bowl1.append("double")

def remove_player(update, ctx):
    bowl1, bowl2 = reset_bowls(ctx)

    command_and_name = update.message.text.strip().split(" ")
    if len(command_and_name) < 2 or command_and_name[1] == "" or command_and_name[1] == "double":
        update.message.reply_text("please send me a name after /remove")
        return

    name = command_and_name[1]
    if name in bowl1:
        bowl1.remove(name)
        update.message.reply_text("'{}' has been removed from play".format(name))
        n_doubles = bowl1.count("double")
        if n_doubles > (len(bowl1) - n_doubles) // 2:
            bowl1.remove("double")
    else:
        update.message.reply_text("'{}' is not playing".format(name))

def draw_names(update, ctx):
    bowl1, bowl2 = get_bowls(ctx)

    bowl, other_bowl = (bowl1, bowl2) if len(bowl1) > 0 else (bowl2, bowl1)
    if len(bowl) in [0, 1]:
        update.message.reply_text("please /add players first!")
        return

    name = random.choice(bowl)
    bowl.remove(name)
    other_bowl.append(name)
    if name == "double":
        names = []
        while len(names) < 2:
            n = random.choice(bowl)
            bowl.remove(n)
            other_bowl.append(n)
            if n == "double":
                continue
            names.append(n)
        update.message.reply_text("double! {} and {}".format(*names))
    else:
        update.message.reply_text(name)
            
    if len(bowl) - bowl.count("double") < 2:
        other_bowl.extend(bowl)
        bowl[:] = []


def run_bot():
    """Create logger, book bot handlers, start listening to messages."""
    log_fmt = '%(asctime)s - %(name)s - %(levelname)s - %(message)s'
    logging.basicConfig(format=log_fmt, level=logging.INFO)

    updater = Updater(token='1118644062:AAGKu0wko12k1Ggb9M3hdO80UfUREiklbnM', use_context=True)
    d = updater.dispatcher
    d.add_handler(CommandHandler('start', start))
    d.add_handler(CommandHandler('add', add_player))
    d.add_handler(CommandHandler('remove', remove_player))
    d.add_handler(CommandHandler('draw', draw_names))

    updater.start_polling()
    updater.idle()

if __name__ == "__main__":
    run_bot()
